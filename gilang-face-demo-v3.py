# common library
import time
import os
import shutil

# for web ui
import gradio as gr

# for numerical python
import numpy as np

# opencv2
import cv2

# for mask detector
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model

# deepface library
from deepface import DeepFace

from LivenessDetector import LivenessDetector


# configuration
base_folder_path = os.getcwd()
faces_db_path_in = os.path.join(base_folder_path, "database")
model_name_in = "Facenet512"
model_detector_backend_in = "retinaface"
distance_metric_in = "cosine"
face_mask_detector_model = os.path.join(base_folder_path, "face_mask_detector_model", "mask_detector.model")
liveness_detector_model = os.path.join(base_folder_path, "liveness_model", "liveness_detector.h5")

# global var
is_recognition_started = True
MASK_DETECTOR_OBJ = None
LIVENESS_DETECTOR_OBJ = None

class MaskDetector:

    def __init__(self, face_mask_model="mask_detector.model"):
        try:
            print(f"Loading mask detect model from {face_mask_model}")
            self._model = load_model(face_mask_model)
            self.is_initialize = self._model is not None
            print("Mask model loaded succesfully")
        except Exception as err:
            print(f"Error : {err}")
            self.is_initialize = False

    def detect(self, face_image_input):
        try:
            # print(type(image_input))
            # extract the face ROI, convert it from BGR to RGB channel
            # ordering, resize it to 224x224, and preprocess it
            face = face_image_input.copy()            
            face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
            face = cv2.resize(face, (224, 224))
            face = img_to_array(face)                
            face = preprocess_input(face)
            face = np.expand_dims(face, axis=0)

            # pass the face through the model to determine if the face
            # has a mask or not
            (mask, withoutMask) = self._model.predict(face)[0]
            
            is_using_mask = mask > withoutMask
        except Exception as err:
            print(f"Detect mask failed : {err}")
            is_using_mask = False
            mask, withoutMask = 0, 0
            
        return is_using_mask, mask, withoutMask

def detect_mask_on_face_region(face_regions, image_input=None):
    try:
        for faces in face_regions:            
            # print(faces["x"], faces["y"], faces["w"], faces["h"])
            startY = faces["y"]
            endY = startY + faces["h"]
            startX = faces["x"]
            endX = startX + faces["w"]
            if image_input is not None:
                img_in_mask = image_input[startY:endY, startX:endX]
            # cv2.imwrite("fx.jpg", img_in_mask)
            
            result = MASK_DETECTOR_OBJ.detect(img_in_mask)
            print(f"{result}")
            faces.update({
                "mask": result
            })
    except Exception as err:
        print(f"Error : {err}")
        

def plot_face_regions(image_in, list_of_face_regions=[], line_color=(0, 0, 255)):
    """
    Plot list_of_face_regions onto image_in. This will override the original image.
    :param image_in: Image array to plot
    :param list_of_face_regions: List of faces region with form {'x': , 'y': , 'w': , 'h': }
    :param line_color: Line color in RGB
    """
    try:
        for region in list_of_face_regions:
            face_name = region.get('name', '')
            x1 = region.get('x', 0)
            y1 = region.get('y', 0)
            w = region.get('w', 0)
            h = region.get('h', 0)
            x2 = x1 + w
            y2 = y1 + h
            
            mask_status = region.get('mask',(False, 0, 0))
            is_masked = mask_status[0]
            print(f"{face_name} = {mask_status}")
            image_info = f"{face_name}({'MASK' if is_masked else 'NO MASK'})"
            
            cv2.rectangle(image_in, (x1, y1), (x2, y2), line_color, 2)
            cv2.putText(
                image_in,
                image_info,
                (x1, y1 - 10),
                cv2.FONT_HERSHEY_SIMPLEX,
                1,
                line_color,
                2,
            )
            
            
    except:
        pass


def do_face_recognition(im):
    # global is_recognition_started
    is_recognition_started = True
    t_start = time.time()

    try:
        if is_recognition_started:
            # face recognition
            img_pixel = im
            
            # detect liveness
            LIVENESS_DETECTOR_OBJ.detect(img_pixel)
            is_alive = LIVENESS_DETECTOR_OBJ.liveness_value >= 0.9

            if is_alive:
                dfs, fcs = DeepFace.find(img_path=img_pixel,
                                        model_name=model_name_in,
                                        db_path=faces_db_path_in,
                                        detector_backend=model_detector_backend_in,
                                        distance_metric=distance_metric_in,
                                        silent=True,
                                        include_all_faces_extracted=True
                                        )

                print("Find and recognize = {}".format(time.time() - t_start))

                # process faces
                faces_obj = []
                for df in dfs:
                    # pprint(df)
                    if not df.empty:
                        # process the shortest
                        lbl = df['identity'][0]
                        obj_name = lbl.replace('\\', '/').split('/')[-2]
                        obj_x_pos = df['source_x'][0]
                        obj_y_pos = df['source_y'][0]
                        obj_width = df['source_w'][0]
                        obj_height = df['source_h'][0]

                        # x1 = obj_x_pos
                        # y1 = obj_y_pos
                        # x2 = obj_x_pos + obj_width
                        # y2 = obj_y_pos + obj_height
                        # print(obj_name)

                        face_result = {
                            'name': obj_name,
                            'x': obj_x_pos,
                            'y': obj_y_pos,
                            'w': obj_width,
                            'h': obj_height
                        }
                        print(face_result)

                        faces_obj.append(face_result)

                        # # draw it
                        # cv2.rectangle(img_pixel, (x1, y1), (x2, y2), (0, 0, 255), 2)
                        # cv2.putText(
                        #     img_pixel,
                        #     obj_name,
                        #     (x1, y1 - 10),
                        #     cv2.FONT_HERSHEY_SIMPLEX,
                        #     1,
                        #     (0, 255, 0),
                        #     2,
                        # )
                        
                # detect mask
                detect_mask_on_face_region(fcs, im)

                # plot all faces extracted
                plot_face_regions(img_pixel, fcs, (255, 0, 0))

                # plot face detected
                plot_face_regions(img_pixel, faces_obj, (0, 255, 0))
            else:
                # Display the liveness score in top left corner     
                cv2.putText(img_pixel, "FAKE FACE!!!", (30, 50), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 0, 0), 1)

            t_start = time.time() - t_start
            print("Processing time = {}".format(t_start))
        else:
            time.sleep(1)
            img_pixel = None
    except Exception as err:
        img_pixel = None
        
    return img_pixel

def force_retrain():
    try:
        pkl_name = os.path.join(base_folder_path,"database","representations_facenet512.pkl")
        if os.path.exists(pkl_name):
            print(f"Deleting {pkl_name}")
            os.remove(pkl_name)
            print(f"{pkl_name} deleted")
            
        # call a dummy find function for db_path once to create embeddings in the initialization
        DeepFace.find(
            img_path=np.zeros([224, 224, 3]),
            db_path=faces_db_path_in,
            model_name=model_name_in,
            detector_backend=model_detector_backend_in,
            distance_metric=distance_metric_in,
            enforce_detection=False,
        )
    except Exception as err:
        print(f"Error = {err}")

def copy_file_to_database_folder(sample_name, file_to_copy):
    try:
        db_path_name = os.path.join(base_folder_path,"database",sample_name)
        file_target = os.path.join(db_path_name, os.path.basename(file_to_copy))
        os.makedirs(db_path_name, exist_ok=True)        
        result = shutil.copy(file_to_copy, file_target) == file_target
    except Exception as err:
        file_target = "?"
        print(f"Error : {err}")
        result = False

    print(f"Copying {file_to_copy} to {file_target} : {result}")

def file_upload_handler(sample_name="", file_list=None):
    try:
        if len(sample_name.strip())==0:
            print("Name cannot be empty!!!")
            # file_list.clear()
            raise gr.Error("Name cannot be empty!!!")        
        else:
            # print(dir(file_list[0]))
            # fx = file_list[0]
            # print(fx.file, fx.name, fx.orig_name)
            for filex in file_list:
                # print(f"Copying {filex.orig_name} : {copy_file_to_database_folder(sample_name, filex.orig_name)}")
                copy_file_to_database_folder(sample_name, filex.name)
            # file_list.clear()
            return None, None
    except:
        pass    

def change_working_mode(input_mode):
    try:
        # print(input_mode)
        global is_recognition_started
        # print(dir(cam_output))

        current_status = "live" in input_mode.lower()
        # print(is_recognition_started, current_status)
        is_recognition_started = current_status
        # print(is_recognition_started, current_status)
        # return gr.update(streaming=())    

        # if current_status:
        #     return gr.Webcam(streaming=True).style(height=480, width=640)    
        # else:
        #     return gr.Webcam(streaming=False).style(height=480, width=640)   
        print(f"Camera streaming started = {current_status}")
        # return gr.update(streaming=current_status)
        
        # from https://github.com/Mikubill/sd-webui-controlnet/blob/241c05f8c9d3c5abe637187e3c4bb46f17447029/scripts/controlnet.py#L202-L210
        return {"streaming":current_status, "__type__": "update"}
    except:
        return {"value":None, "streaming":False, "__type__": "update"}      
        
MASK_DETECTOR_OBJ = MaskDetector(face_mask_detector_model)    
LIVENESS_DETECTOR_OBJ = LivenessDetector(liveness_detector_model)    
        
# web ui
with gr.Blocks() as demo_web:
    gr.Markdown("# AI Machine Web Demo")

    with gr.Tab("Upload Data"):
        input_txt_name = gr.Textbox(label="Name")
        input_img_list = gr.File(label="Foto", file_count="multiple", file_types=["image"])
        input_btn_retrain = gr.Button(value="Register")

        input_img_list.upload(file_upload_handler, [input_txt_name, input_img_list],[input_txt_name, input_img_list])
        input_btn_retrain.click(force_retrain)

    with gr.Tab("Photo"):
        with gr.Row().style(equal_height=True):
            input_img_photo = gr.Image().style(height=480, width=640)
            output_img_photo = gr.Image().style(height=480, width=640)

            input_img_photo.change(
                do_face_recognition,
                input_img_photo,
                output_img_photo,
                "image_face_recognition"
            )

    with gr.Tab("Camera"):
        with gr.Column():
            with gr.Row().style(equal_height=True):
                input_cam_photo = gr.Image(source="webcam", streaming=True).style(height=480, width=640)
                # input_cam_photo = gr.Webcam(streaming=True).style(height=480, width=640)
                output_cam_photo = gr.Image(interactive=False).style(height=480, width=640)                

            with gr.Row().style(equal_height=True):
                cb_work_mode = gr.Radio(["Live", "Register"], label="Mode", info="Working mode", value="Live")

            input_cam_photo.change(
                do_face_recognition,
                input_cam_photo,
                output_cam_photo,
                "cam_face_recognition",
                show_progress=False
            )

            cb_work_mode.change(change_working_mode, [cb_work_mode],[input_cam_photo])

        
# build the model in advance
DeepFace.build_model(model_name=model_name_in)

# call a dummy find function for db_path once to create embeddings in the initialization
DeepFace.find(
    img_path=np.zeros([224, 224, 3]),
    db_path=faces_db_path_in,
    model_name=model_name_in,
    detector_backend=model_detector_backend_in,
    distance_metric=distance_metric_in,
    enforce_detection=False,
)

# img_result = do_face_recognition(cv2.imread('facekum.jpg'))
# cv2.imwrite("mk.jpg", img_result)

# launch the demo web
try:
    # use this if you launch locally on your pc
    demo_web.launch(share=False,
                    server_name="0.0.0.0",
                    ssl_keyfile="localhost.key",
                    ssl_certfile="localhost.crt")

    # use this for colab google
    # demo_web.launch(share=False,debug=True)
except KeyboardInterrupt:
    print("\nClosing server...")
    demo_web.close()
    print("Server closed. Bye.............")        