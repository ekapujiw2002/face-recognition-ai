import time
import gradio as gr
import numpy as np
import cv2
from deepface import DeepFace

# method name
# img_path_in = "faces5.jpg"
faces_db_path_in = "database"
model_name_in = "Facenet512"
model_detector_backend_in = "retinaface"
distance_metric_in = "cosine"


def plot_face_regions(image_in, list_of_face_regions=[], line_color=(0, 0, 255)):
    """
    Plot list_of_face_regions onto image_in. This will override the original image.
    :param image_in: Image array to plot
    :param list_of_face_regions: List of faces region with form {'x': , 'y': , 'w': , 'h': }
    :param line_color: Line color in RGB
    """
    for region in list_of_face_regions:
        face_name = region.get('name', '')
        x1 = region.get('x', 0)
        y1 = region.get('y', 0)
        w = region.get('w', 0)
        h = region.get('h', 0)
        x2 = x1 + w
        y2 = y1 + h
        cv2.rectangle(image_in, (x1, y1), (x2, y2), line_color, 2)
        cv2.putText(
            image_in,
            face_name,
            (x1, y1 - 10),
            cv2.FONT_HERSHEY_SIMPLEX,
            1,
            line_color,
            2,
        )


def do_face_recognition(im):
    img_pixel = im
    t_start = time.time()

    try:
        # face recognition
        dfs, fcs = DeepFace.find(img_path=img_pixel,
                                 model_name=model_name_in,
                                 db_path=faces_db_path_in,
                                 detector_backend=model_detector_backend_in,
                                 distance_metric=distance_metric_in,
                                 silent=True,
                                 include_all_faces_extracted=True
                                 )

        print("Find and recognize = {}".format(time.time() - t_start))

        # process faces
        faces_obj = []
        for df in dfs:
            # pprint(df)
            if not df.empty:
                # process the shortest
                lbl = df['identity'][0]
                obj_name = lbl.replace('\\', '/').split('/')[-2]
                obj_x_pos = df['source_x'][0]
                obj_y_pos = df['source_y'][0]
                obj_width = df['source_w'][0]
                obj_height = df['source_h'][0]

                # x1 = obj_x_pos
                # y1 = obj_y_pos
                # x2 = obj_x_pos + obj_width
                # y2 = obj_y_pos + obj_height
                # print(obj_name)

                face_result = {
                    'name': obj_name,
                    'x': obj_x_pos,
                    'y': obj_y_pos,
                    'w': obj_width,
                    'h': obj_height
                }
                print(face_result)

                faces_obj.append(face_result)

                # # draw it
                # cv2.rectangle(img_pixel, (x1, y1), (x2, y2), (0, 0, 255), 2)
                # cv2.putText(
                #     img_pixel,
                #     obj_name,
                #     (x1, y1 - 10),
                #     cv2.FONT_HERSHEY_SIMPLEX,
                #     1,
                #     (0, 255, 0),
                #     2,
                # )

        # plot all faces extracted
        plot_face_regions(img_pixel, fcs, (255, 0, 0))

        # plot face detected
        plot_face_regions(img_pixel, faces_obj, (0, 255, 0))
    except Exception as err:
        pass

    t_start = time.time() - t_start
    print("Processing time = {}".format(t_start))

    return img_pixel


with gr.Blocks() as demo_web:
    gr.Markdown("# AI Machine Web Demo")

    with gr.Tab("Photo"):
        input_img_photo = gr.Image()
        output_img_photo = gr.Image()

        input_img_photo.change(
            do_face_recognition,
            input_img_photo,
            output_img_photo,
            "image_face_recognition"
        )

    with gr.Tab("Camera"):
        input_cam_photo = gr.Image(source="webcam", streaming=True)
        output_cam_photo = gr.Image(interactive=False)

        input_cam_photo.change(
            do_face_recognition,
            input_cam_photo,
            output_cam_photo,
            "cam_face_recognition",
            show_progress=False
        )

# build the model in advance
DeepFace.build_model(model_name=model_name_in)

# call a dummy find function for db_path once to create embeddings in the initialization
DeepFace.find(
    img_path=np.zeros([224, 224, 3]),
    db_path=faces_db_path_in,
    model_name=model_name_in,
    detector_backend=model_detector_backend_in,
    distance_metric=distance_metric_in,
    enforce_detection=False,
)

# img_out = flip_image("faces1.jpg")
# cv2.imshow("Result", img_out)
# cv2.waitKey()
# cv2.destroyAllWindows()

# if __name__ == "__main__":
demo_web.launch(share=False,
                server_name="0.0.0.0",
                ssl_keyfile="localhost.key",
                ssl_certfile="localhost.crt")

# demo_web.launch(share=True,
#                 debug=False,
#                 prevent_thread_lock=False)
