"""
liveness model
"""

import cv2
import numpy as np 

# from keras.models import model_from_json
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv3D, MaxPooling3D
# from keras import backend as K


class LivenessDetector:

    def __init__(self, liveness_model="liveness.h5"):
        try:
            self._list_frames = []
            self.liveness_value = 0.0
            print("Building liveness model")
            self._model = self.get_liveness_model()
            print(f"Loading liveness model from {liveness_model} ...")
            self._model.load_weights(liveness_model)  
            print("Loading model success")
            self.initialize = True
        except Exception as err:
            print(f"Error : {err}")
            self.initialize = False
            
    def get_liveness_model(self):
        try:
            model = Sequential()
            model.add(Conv3D(32, kernel_size=(3, 3, 3),
                            activation='relu',
                            input_shape=(24,100,100,1)))
            model.add(Conv3D(64, (3, 3, 3), activation='relu'))
            model.add(MaxPooling3D(pool_size=(2, 2, 2)))
            model.add(Conv3D(64, (3, 3, 3), activation='relu'))
            model.add(MaxPooling3D(pool_size=(2, 2, 2)))
            model.add(Conv3D(64, (3, 3, 3), activation='relu'))
            model.add(MaxPooling3D(pool_size=(2, 2, 2)))
            model.add(Dropout(0.25))
            model.add(Flatten())
            model.add(Dense(128, activation='relu'))
            model.add(Dropout(0.5))
            model.add(Dense(2, activation='softmax'))
        except:
            model = None

        return model

    def detect(self, frame_in):
        try:
            frame = frame_in.copy()
            liveimg = cv2.resize(frame, (100,100))
            liveimg = cv2.cvtColor(liveimg, cv2.COLOR_BGR2GRAY)
            self._list_frames.append(liveimg)
                
            if len(self._list_frames) >= 24:
                inp = np.array([self._list_frames[-24:]])
                inp = inp/255
                inp = inp.reshape(1,24,100,100,1)
                pred_value = self._model.predict(inp)
                self.liveness_value = pred_value[0][0]
                self._list_frames = self._list_frames[-25:]
        except:
            pass
            
            
if __name__ == "__main__":
    font = cv2.FONT_HERSHEY_DUPLEX
    video_capture = cv2.VideoCapture(0)
    video_capture.set(3, 640)
    video_capture.set(4, 480)
    
    LIVENESS_DETECTOR_OBJ = LivenessDetector("liveness_model/liveness_detector.h5")
    
    while True:
        ret, frame = video_capture.read()
        if ret:            
            # Display the liveness score in top left corner     
            cv2.putText(frame, str(LIVENESS_DETECTOR_OBJ.liveness_value), (20, 20), font, 1.0, (0, 255 if LIVENESS_DETECTOR_OBJ.liveness_value >= 0.95 else 0, 255 if LIVENESS_DETECTOR_OBJ.liveness_value < 0.95 else 0), 1)
        
            # Display the resulting image
            cv2.imshow('Video', frame)
            
            LIVENESS_DETECTOR_OBJ.detect(frame)

        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release handle to the webcam
    video_capture.release()
    cv2.destroyAllWindows()
            
            
            
        

